build: tailwind
	zola build
tailwind:
	npx tailwind -i styles/style.css -o static/style.css
tailwind-watch:
	npx tailwind -i styles/style.css -o static/style.css --watch
serve:
	zola serve