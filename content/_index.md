---
title: spruett.net
---

# hi, i'm scott pruett

i'm a software developer. i build [distributed systems](https://atscaleconference.com/2021/03/15/flighttracker/),
databases, and [fun](https://git.spruett.net/scott/supper) [tools](https://gitlab.com/xenonsh/xejail).
i also play [guitar](https://soundcloud.com/spruett3), d&d, and various other games.

<br/>

feel free to check out my [projects](/projects) or contact me via [email](mailto:contact@spruett.net).