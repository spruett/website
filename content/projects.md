+++
+++

# software
 - [alfie.photos](https://alfie.photos): pictures of my dog in website format
 - [supper](https://git.spruett.net/scott/supper): OCR-based stat tracker for [Circuit Superstars](https://www.circuit-superstars.com/)
 - [pcs](https://pcs.cs.cloud.vt.edu): a competitive programming contest system with automatic judging and scoreboards, written when I was an undergrad at Virginia Tech for our [ACM-ICPC](https://icpc.global/) team
 - [cpr](https://gitlab.com/spruett/cpr): an implementation of the [language server protocol](https://microsoft.github.io/language-server-protocol/) for C++, using heuristics instead of a full compiler
 - [xejail](https://gitlab.com/xenonsh/xejail): a sandboxing utility meant for running single binaries for judging or grading
 - [chordgen](https://chordgen.xyz): web app that shows you how to play chords on guitar
 - [wordle grader](https://wordy.spruett.net): tiny website which shows you how poorly you did at [Wordle](https://www.nytimes.com/games/wordle/index.html)
 - [this website](https://gitlab.com/spruett/website)
# other
 - [brought to the table](https://broughttothetable.com): a podcast I did with my college friends
 - [music recordings](https://soundcloud.com/spruett3): some of my more finished guitar or voice recordings are on SoundCloud
