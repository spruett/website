+++
+++

# work

I currently work at [Cruise](https://getcruise.com/) on scheduling infrastructure which supports
simulations.
<br/>

In the past I've worked at:
 * Meta working on distributed cache consistency and replication, such as
   [FlightTracker](https://www.usenix.org/conference/osdi20/presentation/shi),
   a novel approach to providing read-your-writes consistency at scale
 * [Yale](https://www.yale.edu/) as a lecturer for [CPSC 426/526](https://github.com/shixiao/cs426-spring2022-labs/)
  (Building Distributed systems) with Professor Richard Yang and my colleague [Xiao Shi](https://www.shixiao.org/)
 * [Genworth Financial](https://www.genworth.com/) as an intern

# education

I graduated from [Virginia Tech](https://vt.edu/) with B.S. in Computer Science in 2016.
I minored in math and physics.